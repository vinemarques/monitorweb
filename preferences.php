<?php
session_start();
function refresh($site){
    echo "<script type=\"text/javascript\">window.location.href = '".$site."';</script>";
}
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="./assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="./assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="./assets/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="assets/css/style_login.css">
    <link rel="stylesheet" type="text/css" href="./assets/css/unstyle.css">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Colégio Técnico de Limeira</title>
</head>
<body>
<?php

if(!isset($_SESSION['cotil_user_status'])) {
    refresh("login.php");
    exit();
} else {
    if($_SESSION['cotil_user_status']==0){ // nunca entrou
        refresh("./first/first.php");
        exit();
    } else if($_SESSION['cotil_user_status']==2) { //confirmação faltando
        refresh("./first/firstfinish.php");
        exit();
    } else if($_SESSION['cotil_user_status']==1) { //email confirmado
        require_once './assets/include/header-logged.inc';
    } else { // erro cabuloso, status dif 0 1 2
        require_once './assets/include/header-def.inc';
        echo "<div class='container-fluid' id='main-container' style='padding-left: 0%; padding-right: 0%;'> <br>
                <div class='alert alert-danger'>
                     <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                     <strong>Ops!</strong> Há algo de errado com a sua conta. Verifique a situação da sua conta na direção ou com um monitor.
                  </div>
                  <br><br><br><br><br><br><br>
                  <center><a href='./assets/logout.php'><button type='button' class='btn btn-danger'>Sair</button></a></center>
                  <br><br><br><br><br><br><br><br>
                  ";
        exit();
    }
}
?>

    <div class='container-fluid' id='main-container'>
        <div class='jumbotron' style='background-color: transparent; padding-right: 10%; padding-left: 10%'>
            <br><center><h3>Preferências da Conta</h3></center><br><br>
            <div style='text-align: justify;'>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pellentesque ligula at metus tincidunt eleifend. Proin et quam ante. Mauris commodo lorem vehicula nisl iaculis consectetur. Vestibulum a aliquet nulla. Sed volutpat felis vel sapien condimentum aliquet. Fusce non fringilla sapien. Pellentesque at augue nec purus sodales pulvinar. Interdum et.</p>
            </div>
        </div>
    </div>

<?php include './assets/include/footer.inc';?>
<!--TODO: Refazer o footer, achar outro design daorinha-->
<script src="./assets/js/jquery-3.1.1.js"></script>
<script src="./assets/js/tether.js"></script>
<script src="./assets/js/bootstrap.js"></script>
</body>
</html>