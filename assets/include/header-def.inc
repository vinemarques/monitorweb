<header xmlns:padding="http://www.w3.org/1999/xhtml" xmlns:display="http://www.w3.org/1999/xhtml">
	  <nav class="navbar navbar-light navbar-toggler-icon navbar-fixed-top" role="banner" style="border-radius:0px; background-color:#ebebeb;">
          <div class="row"  style="display: flex; padding: 0px 0px 0px 0px;">

              <div class="col-md-3 col-sm-12">
                  <div class="nav-item">
                  <a href="http://www.unicamp.br/" class="navbar-brand" target="_blank">
                       <img src="../assets/img/unicamp-logo.png" style="width: 35px; height: auto;" class="d-inline-block align-top">
                  </a>
                  <a href="../" class="navbar-brand">
                       <img src="../assets/img/cotil-logo.png" style="width: 80px; height: auto;" class="d-inline-block align-top">
                  </a>
                  </div>
              <button id="nav-toggle-button" class="navbar-toggler hidden-md-up collapsed" type="button" data-toggle="collapse" style="float: right" data-target="#menu-sanduiche" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
              </button>
              </div>


              <div class="col-md-9 col-sm-0">
                  <div class="navbar-toggleable-sm collapse" id="menu-sanduiche" style="float:left;" aria-expanded="false">

                      <ul class="nav navbar-nav">
                          <li class="nav-item dropdown">
                              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">O Colégio</a>
                              <div class="dropdown-menu">
                                  <a href="../nossahistoria.php" class="dropdown-item">Nossa História</a>
                                  <a href="../estrutura.php" class="dropdown-item">Estrutura</a>
                                  <a href="https://www.fca.unicamp.br/portal/institucional/restaurante/cardapio.html" class="dropdown-item">Cardápio RU</a>
                                  <a href="../mapa.php" class="dropdown-item">Localização</a>
                                  <a href="../adm.php" class="dropdown-item">Administração</a>
                                  <a href="http://www.sbu.unicamp.br/portal2/" class="dropdown-item">Biblioteca Online</a>
                                  <!--<a href="./apm.php" class="dropdown-item">A.P.M.</a>-->
                                  <a href="../cotilarte.php" class="dropdown-item">COTIL Arte</a>
                                  <a href="../contato.php" class="dropdown-item">Contato</a>
                              </div>
                          </li>

                          <li class="nav-item dropdown">
                              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Cursos Técnicos</a>
                              <div class="dropdown-menu">
                                  <a href="../info.php" class="dropdown-item">Informática</a>
                                  <a href="../mec.php" class="dropdown-item">Mecânica</a>
                                  <a href="../enf.php" class="dropdown-item">Enfermagem</a>
                                  <a href="http://www.cotil.unicamp.br/dcgeo/index.html" class="dropdown-item">Edificações</a>
                                  <a href="http://www.cotil.unicamp.br/dcgeo/index.html" class="dropdown-item">Geodésia e Cartografia</a>
                                  <a href="../qua.php" class="dropdown-item">Qualidade</a>
                              </div>
                          </li>

                          <li class="nav-item dropdown">
                              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Acadêmico</a>
                              <div class="dropdown-menu">
                                  <a href="http://www.cotil.unicamp.br/estagio/index.php" class="dropdown-item">Estágios</a>
                                  <a href="../solicitadoc.php" class="dropdown-item">Solicitação de Documentos</a>
                                  <a href="http://www.cotil.unicamp.br/arquivos/calendario_academico2017.pdf" class="dropdown-item">Calendário Acadêmico</a>
                                  <a href="../aprovUniv.php" class="dropdown-item">Aprovados em Universidades</a>
                                  <a href="http://www.cotil.unicamp.br/horario_aulas.php" class="dropdown-item">Horários das Aulas</a>
                                  <a href="http://www.cotil.unicamp.br/arquivos/manual_do_aluno.pdf" class="dropdown-item">Manual do Aluno</a>
                              </div>
                          </li>

                          <li class="nav-item">
                              <a href="../servicos.php" class="nav-link">Serviços</a>
                          </li>

                          <li class="nav-item">
                              <a href="../concursos.php" class="nav-link">Concursos</a>
                          </li>

                          <li class="nav-item">
                              <a href="index.php" class="nav-link">Monitorias</a>
                          </li>

                          <!--<li class="nav-item">
                              <a href="http://www.sbu.unicamp.br/portal2/" class="nav-link">Biblioteca Online</a>
                          </li>-->

                          <li class="nav-item">
                              <a href="http://www.secretaria.cotil.unicamp.br/sg_web/acesso.aspx?escola=3307" class="nav-link"><b>Portal</b></a>
                          </li>

                          <li class="nav-item">
                              <a href="http://www.exame.cotil.unicamp.br/" class="nav-link" style="color: #ED3237"><b>Vestibulinho</b></a>
                          </li>
                      </ul>

                  </div>
              </div>
          </div>
      </nav>
  </header>
