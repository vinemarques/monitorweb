<header xmlns:padding="http://www.w3.org/1999/xhtml" xmlns:display="http://www.w3.org/1999/xhtml">
	  <nav class="navbar navbar-light navbar-toggler-icon navbar-fixed-top" role="banner" style="border-radius:0px; background-color:#ebebeb;">
          <div class="row"  style="display: flex; padding: 0px 0px 0px 0px;">

              <div class="col-md-3 col-sm-12">
                  <div class="nav-item">
                  <a href="http://www.unicamp.br/" class="navbar-brand" target="_blank">
                       <img src="assets/img/unicamp-logo.png" style="width: 35px; height: auto;" class="d-inline-block align-top">
                  </a>
                  <a href="../" class="navbar-brand">
                       <img src="assets/img/cotil-logo.png" style="width: 80px; height: auto;" class="d-inline-block align-top">
                  </a>
                  </div>
              <button id="nav-toggle-button" class="navbar-toggler hidden-md-up collapsed" type="button" data-toggle="collapse" style="float: right" data-target="#menu-sanduiche" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
              </button>
              </div>


              <div class="col-md-9 col-sm-0">
                  <div class="navbar-toggleable-sm collapse" id="menu-sanduiche" style="float:left;" aria-expanded="false">

                      <ul class="nav navbar-nav">
                          <li class="nav-item">
                              <a href="index.php" class="nav-link">Home</a>
                          </li>

                          <li class="nav-item dropdown">
                              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Horários</a>
                              <div class="dropdown-menu">
                                  <?php if(isset($_SESSION['cotil_user_tec'])) {echo "<a href='#' class='dropdown-item'>Técnico em ".$_SESSION['cotil_user_tec']."</a>";} ?>
                                  <?php if(isset($_SESSION['cotil_user_sala'])) {echo "<a href='#' class='dropdown-item'>Ensino Médio</a>";} ?>
                              </div>
                          </li>

                          <li class="nav-item dropdown">
                          <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Agendar Monitoria</a>
                          <div class="dropdown-menu">
                              <?php
                              if(isset($_SESSION['cotil_user_tec'])) {
                                  echo "<a href='./schedule_tecnico.php' class='dropdown-item'>Técnico em ".$_SESSION['cotil_user_tec']."</a>";
                              }
                              if(isset($_SESSION['cotil_user_sala'])) {
                                  echo "<a href='./schedule_medio.php' class='dropdown-item'>Ensino Médio</a>";
                              }
                              ?>
                          </div>
                          </li>

                          <li class="nav-item dropdown">
                              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Desmarcar Monitoria</a>
                              <div class="dropdown-menu">
                                  <?php
                                  if(isset($_SESSION['cotil_user_tec'])) {
                                      echo "<a href='./markoff_tecnico.php' class='dropdown-item'>Técnico em ".$_SESSION['cotil_user_tec']."</a>";
                                  }
                                  if(isset($_SESSION['cotil_user_sala'])) {
                                      echo "<a href='./markoff_medio.php' class='dropdown-item'>Ensino Médio</a>";
                                  }
                                  ?>
                              </div>
                          </li>

                          <li class="nav-item">
                              <a href="../index.php" class="nav-link"><b>Retornar ao site</b></a>
                          </li>

                          <li class="nav-item dropdown">
                              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="color: #ED3237"><i class="fa fa-user" aria-hidden="true"></i> <b><?php echo explode(' ',$_SESSION['cotil_user_nome'])[0]; ?></b></a>
                              <div class="dropdown-menu">
                                  <a href="./profile.php" class="dropdown-item"><i class="fa fa-user" aria-hidden="true"></i> Perfil</a>
                                  <a href="./preferences.php" class="dropdown-item"><i class="fa fa-cog" aria-hidden="true"></i> Preferências</a>
                                  <div class="dropdown-divider"></div>
                                  <a href="./assets/logout.php" class="dropdown-item"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>

                              </div>
                          </li>
                      </ul>

                  </div>
              </div>
          </div>
      </nav>
  </header>
<!-- TODO: Refazer toda a navbar p/ aluno -> monitor depois -> professor e direção dps -->