<?php

    if (isset($_REQUEST['fauth']) && $_REQUEST['fauth'] == true) {

        require_once './assets/include/flog/autoload.php';
        require_once './assets/include/connection.inc';

        $fb = new Facebook\Facebook([
            'app_id' => '250718605417966',
            'app_secret' => '9c3a43656137ece05608597b97ee82e9',
            'default_graph_version' => 'v2.9',
        ]);

        $login=$fb->getRedirectLoginHelper();
        $permissions = ['email'];
        try {
            if (isset($_SESSION['facebook_access_token'])) {
                $accessToken = $_SESSION['facebook_access_token'];
            } else {
                $accessToken = $login->getAccessToken();
            }
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        if (isset($accessToken)) {
            if (isset($_SESSION['facebook_access_token'])) {
                $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
            } else {
                $_SESSION['facebook_access_token'] = (string) $accessToken;
                $oAuth2Client = $fb->getOAuth2Client();
                $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
                $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
                $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
            }
            /*if (isset($_GET['code'])) {
                header('Location: ./');
            }*/
            try {
                $profile_request = $fb->get('/me?fields=name,first_name,last_name,email');
                $profile = $profile_request->getGraphNode()->asArray();
                //begina

                echo '<div style="margin:20px">';
                    $stmt = $conn->prepare("SELECT *, COUNT(app_id) as usercount FROM Facebook_Auth WHERE app_id=".$profile['id']);

                    if ($stmt->execute()) {
                        $registro = $stmt->fetch(PDO::FETCH_OBJ);
                        if ($user_count = $registro->usercount) {
                            $sql2 = "SELECT * FROM Alunos WHERE ra = :ra";

                            $stmt2 = $conn->prepare($sql2);

                            $stmt2->bindParam("ra", $registro->cotil_ra);

                            if ($stmt2->execute()) {
                                if ($registro2 = $stmt2->fetch(PDO::FETCH_OBJ)) {
                                    $_SESSION['cotil_user_ra'] = $registro2->ra;
                                    $_SESSION['cotil_user_nome'] = $registro2->nome;
                                    $_SESSION['cotil_user_status'] = $registro2->status;
                                    $_SESSION['cotil_user_phone'] = $registro2->telefone;
                                    $_SESSION['cotil_user_mail'] = $registro2->email;
                                    if ($_SESSION['cotil_user_status'] == 0) {
                                        $_SESSION['cotil_lvl'] = 0;
                                        refresh("./first/first.php");
                                        exit();
                                    } else if ($_SESSION['cotil_user_status'] == 2) {
                                        $_SESSION['cotil_lvl'] = 4;
                                        $_SESSION['cotil_ne'] = $registro2->email;
                                        refresh("./first/firstfinish.php");
                                        exit();
                                    } else {
                                        switch ($registro2->curso[1]) {
                                            case 1:
                                                $_SESSION['cotil_user_tec'] = "Informática";
                                                break;
                                            case 2:
                                                $_SESSION['cotil_user_tec'] = "Mecânica";
                                                break;
                                            case 3:
                                                $_SESSION['cotil_user_tec'] = "Edificações";
                                                break;
                                            case 4:
                                                $_SESSION['cotil_user_tec'] = "Geodésia e Cartografia";
                                                break;
                                            case 5:
                                                $_SESSION['cotil_user_tec'] = "Enfermagem";
                                                break;
                                            case 6:
                                                $_SESSION['cotil_user_tec'] = "Qualidade";
                                                break;
                                        }
                                        if ($registro2->sala != 000) {
                                            if ($registro2->sala[0] == 1) {
                                                $_SESSION['cotil_user_sala'] = $registro2->sala[1] . "." . $registro2->sala[2] . " Diurno";
                                                $_SESSION['cotil_periodo'] = 1;
                                            } else {
                                                $_SESSION['cotil_user_sala'] = $registro2->sala[1] . "." . $registro2->sala[2] . " Noturno";
                                                $_SESSION['cotil_periodo'] = 2;
                                            }
                                        }
                                        $_SESSION['cotil_lvl'] = 5;
                                        refresh("index.php");
                                        exit();
                                    }
                                } else {
                                    $_SESSION['preauth']='g';
                                    $_SESSION['preauthid']=$user->id;
                                    refresh("./first/association.php");
                                }
                            } else {
                                echo "Falha de acesso ao BD!<br>";
                            }
                        } else {
                            $statement = $conn->prepare("INSERT INTO Facebook_Auth VALUES (:id,:name,0,:email)");
                            $statement->bindParam("id", $profile['id']);
                            $statement->bindParam("name", $profile['name']);
                            $statement->bindParam("email", $profile['email']);
                            $statement->execute();
                            $_SESSION['preauth']='f';
                            $_SESSION['preauthid']=$profile['id'];
                            refresh("./first/association.php");
                        }
                    }
                echo '</div>';

                //enda
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                session_destroy();
                header("Location: ./");
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            var_dump($profile);
        }else {
            $loginUrl = $login->getLoginUrl('http://lab.vinemarques.tk/infd35/cotil/monitoria/login.php?fauth=true', $permissions);
            refresh($loginUrl);
        }

    }