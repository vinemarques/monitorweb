<?php
session_start(); //session start

require_once ('libraries/Google/autoload.php');

//Insert your cient ID and secret 
//You can get it from : https://console.developers.google.com/
$client_id = '886302792209-bec1m3uo26j9o52b02oj8ce222rchn73.apps.googleusercontent.com';
$client_secret = 'UxBTiUVWJo-t8DfMwdRB8Dgw';
$redirect_uri = 'http://lab.vinemarques.tk/infd35/cotil/monitoria/assets/include/glog/index.php';

//database
require_once '../connection.inc';

//incase of logout request, just unset the session var
if (isset($_GET['logout'])) {
  unset($_SESSION['access_token']);
}

/************************************************
  Make an API request on behalf of a user. In
  this case we need to have a valid OAuth 2.0
  token for the user, so we need to send them
  through a login flow. To do this we need some
  information from our API console project.
 ************************************************/
$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->addScope("email");
$client->addScope("profile");

/************************************************
  When we create the service here, we pass the
  client to it. The client then queries the service
  for the required scopes, and uses that when
  generating the authentication URL later.
 ************************************************/
$service = new Google_Service_Oauth2($client);

/************************************************
  If we have a code back from the OAuth 2.0 flow,
  we need to exchange that with the authenticate()
  function. We store the resultant access token
  bundle in the session, and redirect to ourself.
*/
  
if (isset($_GET['code'])) {
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
  exit;
}

/************************************************
  If we have an access token, we can make
  requests, else we generate an authentication URL.
 ************************************************/
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  $client->setAccessToken($_SESSION['access_token']);
} else {
  $authUrl = $client->createAuthUrl();
}


//Display user info or display login url as per the info we have.
echo '<div style="margin:20px">';
if (isset($authUrl)){ 
	//show login url
	echo '<div align="center">';
	echo '<h3>Login with Google -- Demo</h3>';
	echo '<div>Please click login button to connect to Google.</div>';
	echo '<a class="login" href="' . $authUrl . '"><img src="images/google-login-button.png" /></a>';
	echo '</div>';
	
} else {

    $user = $service->userinfo->get(); //get user info

    // connect to database


    //check if user exist in database using COUNT
    $stmt = $conn->prepare("SELECT *, COUNT(google_id) as usercount FROM Google_Auth WHERE google_id=$user->id");

    if ($stmt->execute()) {
        $registro = $stmt->fetch(PDO::FETCH_OBJ);
        if ($user_count = $registro->usercount) {
            echo "Welcome back " . $registro->google_name . "! <a href='?logout=1'>Logout</a>";
        } else {
            echo 'Hi ' . $user->name . ', Thanks for Registering! [<a href="' . $redirect_uri . '?logout=1">Log Out</a>]';
            $statement = $conn->prepare("INSERT INTO Google_Auth VALUES (:id,:name,:email,:link,:pic)");
            $statement->bindParam("id", $user->id);
            $statement->bindParam("name", $user->name);
            $statement->bindParam("email", $user->email);
            $statement->bindParam("link", $user->link);
            $statement->bindParam("pic", $user->picture);
            $statement->execute();
        }
    }

    //show user picture
    echo '<img src="' . $user->picture . '" style="float: right;margin-top: 33px;" />';

    //print user details
    echo '<pre>';
    print_r($user);
    echo '</pre>';
}
echo '</div>';


?>

