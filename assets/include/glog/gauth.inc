<?php

    if (isset($_REQUEST['gauth']) && $_REQUEST['gauth'] == true) {
    require_once ('assets/include/glog/libraries/Google/autoload.php');

    $client_id = '886302792209-bec1m3uo26j9o52b02oj8ce222rchn73.apps.googleusercontent.com';
    $client_secret = 'UxBTiUVWJo-t8DfMwdRB8Dgw';
    $redirect_uri = 'http://lab.vinemarques.tk/infd35/cotil/monitoria/login.php?gauth=true';

    require_once 'assets/include/connection.inc';

    if (isset($_GET['logout'])) {
    unset($_SESSION['access_token']);
    }

    $client = new Google_Client();
    $client->setClientId($client_id);
    $client->setClientSecret($client_secret);
    $client->setRedirectUri($redirect_uri);
    $client->addScope("email");
    $client->addScope("profile");

    $service = new Google_Service_Oauth2($client);

    if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $_SESSION['access_token'] = $client->getAccessToken();
    refresh($redirect_uri);
    exit;
    }

    if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
    $client->setAccessToken($_SESSION['access_token']);
    } else {
    $authUrl = $client->createAuthUrl();
    }
        if (isset($authUrl)){
        refresh($authUrl);
        } else {
        $user = $service->userinfo->get();

        $stmt = $conn->prepare("SELECT *, COUNT(google_id) as usercount FROM Google_Auth WHERE google_id=$user->id");

        if ($stmt->execute()) {
            $registro = $stmt->fetch(PDO::FETCH_OBJ);
            if ($user_count = $registro->usercount) {
                $sql2 = "SELECT * FROM Alunos WHERE ra = :ra";

                $stmt2 = $conn->prepare($sql2);

                $stmt2->bindParam("ra", $registro->cotil_ra);

                if ($stmt2->execute()) {
                    if ($registro2 = $stmt2->fetch(PDO::FETCH_OBJ)) {
                        $_SESSION['cotil_user_ra'] = $registro2->ra;
                        $_SESSION['cotil_user_nome'] = $registro2->nome;
                        $_SESSION['cotil_user_status'] = $registro2->status;
                        $_SESSION['cotil_user_phone'] = $registro2->telefone;
                        $_SESSION['cotil_user_mail'] = $registro2->email;
                        if ($_SESSION['cotil_user_status'] == 0) {
                            $_SESSION['cotil_lvl'] = 0;
                            refresh("./first/first.php");
                            exit();
                        } else if ($_SESSION['cotil_user_status'] == 2) {
                            $_SESSION['cotil_lvl'] = 4;
                            $_SESSION['cotil_ne'] = $registro2->email;
                            refresh("./first/firstfinish.php");
                            exit();
                        } else {
                            switch ($registro2->curso[1]) {
                                case 1:
                                    $_SESSION['cotil_user_tec'] = "Informática";
                                    break;
                                case 2:
                                    $_SESSION['cotil_user_tec'] = "Mecânica";
                                    break;
                                case 3:
                                    $_SESSION['cotil_user_tec'] = "Edificações";
                                    break;
                                case 4:
                                    $_SESSION['cotil_user_tec'] = "Geodésia e Cartografia";
                                    break;
                                case 5:
                                    $_SESSION['cotil_user_tec'] = "Enfermagem";
                                    break;
                                case 6:
                                    $_SESSION['cotil_user_tec'] = "Qualidade";
                                    break;
                            }
                            if ($registro2->sala != 000) {
                                if ($registro2->sala[0] == 1) {
                                    $_SESSION['cotil_user_sala'] = $registro2->sala[1] . "." . $registro2->sala[2] . " Diurno";
                                    $_SESSION['cotil_periodo'] = 1;
                                } else {
                                    $_SESSION['cotil_user_sala'] = $registro2->sala[1] . "." . $registro2->sala[2] . " Noturno";
                                    $_SESSION['cotil_periodo'] = 2;
                                }
                            }
                            $_SESSION['cotil_lvl'] = 5;
                            refresh("index.php");
                            exit();
                        }
                    } else {
                        $_SESSION['preauth']='g';
                        $_SESSION['preauthid']=$user->id;
                        refresh("./first/association.php");
                    }
                } else {
                    echo "Falha de acesso ao BD!<br>";
                }
            } else {
                $statement = $conn->prepare("INSERT INTO Google_Auth VALUES (:id,:name,0,:email)");
                $statement->bindParam("id", $user->id);
                $statement->bindParam("name", $user->name);
                $statement->bindParam("email", $user->email);
                $statement->execute();
                $_SESSION['preauth']='g';
                $_SESSION['preauthid']=$user->id;
                refresh("./first/association.php");
            }
        }
        }
    }