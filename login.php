<?php
session_start();

function refresh($site){
    echo "<script type=\"text/javascript\">window.location.href = '".$site."';</script>";
}

?>

<!DOCTYPE html>
<html lang="pt-BR" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="./assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="./assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="./assets/css/bootstrap-social.css">
    <link rel="stylesheet" href="./assets/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="assets/css/style_login.css">
    <link rel="stylesheet" type="text/css" href="./assets/css/unstyle.css">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
    <title>Monitoria - Colégio Técnico de Limeira</title>
</head>
<body>
<?php
include './assets/include/header-def.inc';
if (!isset($_SESSION['cotil_user_ra'])) {
    if (isset($_REQUEST['auth']) && $_REQUEST['auth'] == true) {
        include './assets/include/connection.inc';
        $login = isset($_REQUEST['login']) ? $_REQUEST['login'] : null;
        $pass = isset($_REQUEST['pass']) ? $_REQUEST['pass'] : null;
        $pass = md5($pass . "batata");

        $sql = "SELECT * FROM Alunos WHERE ra = :login AND senha = :pass";

        $stmt = $conn->prepare($sql);

        $stmt->bindParam("login", $login);
        $stmt->bindParam("pass", $pass);

        if ($stmt->execute()) {
            if ($registro = $stmt->fetch(PDO::FETCH_OBJ)) {
                //se incluir dados carregados, incluir no fauth e gauth
                $_SESSION['cotil_user_ra'] = $registro->ra;
                $_SESSION['cotil_user_nome'] = $registro->nome;
                $_SESSION['cotil_user_status'] = $registro->status;
                $_SESSION['cotil_user_phone'] = $registro->telefone;
                $_SESSION['cotil_user_mail'] = $registro->email;
                if ($_SESSION['cotil_user_status'] == 0) {
                    $_SESSION['cotil_lvl'] = 0;
                    refresh("./first/first.php");
                    exit();
                } else if ($_SESSION['cotil_user_status'] == 2) {
                    $_SESSION['cotil_lvl'] = 4;
                    $_SESSION['cotil_ne'] = $registro->email;
                    refresh("./first/firstfinish.php");
                    exit();
                } else {
                    switch ($registro->curso[1]) {
                        case 1:
                            $_SESSION['cotil_user_tec'] = "Informática";
                            break;
                        case 2:
                            $_SESSION['cotil_user_tec'] = "Mecânica";
                            break;
                        case 3:
                            $_SESSION['cotil_user_tec'] = "Edificações";
                            break;
                        case 4:
                            $_SESSION['cotil_user_tec'] = "Geodésia e Cartografia";
                            break;
                        case 5:
                            $_SESSION['cotil_user_tec'] = "Enfermagem";
                            break;
                        case 6:
                            $_SESSION['cotil_user_tec'] = "Qualidade";
                            break;
                    }
                    if ($registro->sala != 000) {
                        if ($registro->sala[0] == 1) {
                            $_SESSION['cotil_user_sala'] = $registro->sala[1] . "." . $registro->sala[2] . " Diurno";
                            $_SESSION['cotil_periodo'] = 1;
                        } else {
                            $_SESSION['cotil_user_sala'] = $registro->sala[1] . "." . $registro->sala[2] . " Noturno";
                            $_SESSION['cotil_periodo'] = 2;
                        }
                    }
                    $_SESSION['cotil_lvl'] = 5;
                    refresh("index.php");
                    exit();
                }
            } else {
                echo "<div class='container-fluid' id='main-container' style='padding-left: 0%; padding-right: 0%;'> <br>
                        <div class='alert alert-danger'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>Ops!</strong> Usuário e/ou senha incorretos.
                        </div>
                      </div>";
            }
        } else {
            echo "Falha de acesso ao BD!<br>";
        }
    }

    include 'assets/include/glog/gauth.inc'; // Inclui autenticação do google

    include 'assets/include/flog/fauth.inc'; // Inclui autenticação do facebook

} else {
    refresh("index.php");
}
//TODO: Regra de login adaptada apenas p/ ra, criar logica p/ email e eduroam
?>
<div class='container-fluid' id='main-container'>
    <br>
    <div class="jumbotron" style="text-align: justify">
        <h2 style="text-align: center">Painel de Monitorias</h2>
        <br>
        <section id="card">
            <div class="col-md-12 card-deck card">
                <!-- TODO: Deixar bonita a parte esquerda desse card -->
                <div class="col-md-6 col-sm-0 hidden-md-down">
                    <br><br><br><center><img src="assets/img/logoAcesso.jpg" style="width: auto; height: auto;" class="d-inline-block"><br>
                    <b>Faça o seu login e agende suas monitorias</b></center>
                </div>
                <div class="col-md-6 col-sm-0 hidden-md-up" style="padding-top: 30px;">
                    Faça o seu login e visualize, altere ou agende suas monitorias
                </div>
                <div class="col-sm-6 col-md-6">
                    <br>
                    <form action="?auth=true" method="post" class="form-signin">
                        <div class="form-group">
                            <label for="login"><b>Usuário:</b></label>
                            <input type="text" style="border-color: #ED3237" class="form-control" name="login" id="login" placeholder="Email, RA ou Eduroam" required autofocus>
                        </div>
                        <div class="form-group">
                            <label for="senha"><b>Senha:</b></label>
                            <input type="password" style="border-color: #ED3237" class="form-control" name="pass" id="senha" placeholder="Senha" required>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-lg btn-primary btn-block"  style="background-color: #ED3237; border-color: #ED3237" name="submit" value="Login" type="submit">Entrar</button>
                        </div>
                        <div class="form-group row">
                            <a class="btn btn-social btn-google offset-md-1 col-md-4 col-sm-6" href="?gauth=true">
                                <i class="fa fa-google" style="color: white"></i> <span style="color: white"> Google</span>
                            </a>
                            <a class="btn btn-social btn-facebook offset-md-2 col-md-4 col-sm-6" href="?fauth=true">
                                <i class="fa fa-facebook" style="color: white"></i> <span style="color: white"> Facebook</span>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>

<?php
    include './assets/include/footer.inc';
?>

<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
</body>
</html>