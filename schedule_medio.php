<?php
session_start();
function refresh($site){
    echo "<script type=\"text/javascript\">window.location.href = '".$site."';</script>";
}
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap-flex.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="assets/css/style_login.css">
    <link rel="stylesheet" type="text/css" href="assets/css/unstyle.css">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
    <title>Monitorias - Colégio Técnico de Limeira</title>
</head>
<body>

<?php
if (isset($_SESSION['cotil_periodo'])){
    include './assets/include/header-logged.inc';
} else {
    refresh("login.php");
    exit();
}
?>

<div class='container-fluid' id="main-container">
    <div class="jumbotron" style='background-color: transparent; padding-right: 10%; padding-left: 10%'>
        <br><center><h2>Agendar Monitoria</h2></center><br>
        <div style='text-align: justify;'>
            <p>Selecione abaixo uma materia a qual se deseja agendar uma monitoria e complete atentamente o formulário com os dados solicitados:</p>
            <div class="row">
                <div id="accordion" role="tablist" style="width: 100%;">
                    <div class="card">
                        <div class="card-header" role="tab" id="Node1">
                            <h5 class="mb-0">
                                <a href="#ConteudoNode1" data-toggle="collapse" data-parent="#accordion" class="text-danger">Monitoria de Exatas</a>
                            </h5>
                        </div>
                        <div class="collapse" role="tabpanel" id="ConteudoNode1">
                            <div class="card-block">
                                <form name="estagio" action="#act=estagio">
                                    <div class="form-group">
                                        <label for="doc2"><b>Selecione uma matéria:</b></label>
                                        <select class="form-control" id="doc2" name="doc2" required onchange="updateSelectMateria(this.value,1)">
                                            <option disabled selected value>Selecione uma matéria</option>
                                            <option value="1"> Biologia</option>
                                            <option value="2"> Física</option>
                                            <option value="3"> Matemática</option>
                                            <option value="4"> Química</option>
                                        </select>
                                    </div><br>
                                    <p>
                                        <b>Escolha os itens a seguir conforme a preferência de horário ou de monitor:</b><br>
                                        <small>(A escolha de um monitor limita as escolhas de horário, assim como a escolha de horário limita a escolha de monitores.)</small>
                                    </p>
                                    <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="doc2"><b>Escolha um monitor:</b></label>
                                        <select class="form-control" id="selectmonitor" name="selectmonitor" required>
                                            <option disabled selected value>Selecione primeiramente uma matéria</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="doc2"><b>Escolha um horário:</b></label>
                                        <select class="form-control" id="doc2" name="doc2" required>
                                            <option disabled selected value>Selecione primeiramente uma matéria</option>
                                        </select>
                                    </div>
                                    </div><br>
                                    <div class="form-group">
                                        <label for="nome"><b>Escreva sobre o assunto monitoria:</b></label>
                                        <textarea class="form-control" rows="3" id="comment"></textarea>
                                        <small id="nameHelp" class="form-text text-muted">Cite todas as matérias e temas.</small>
                                    </div>
                                        <div class="form-group">
                                            <label for="phone"><b>Telefone:</b></label>
                                            <input type="phone" class="form-control" id="phone" aria-describedby="phoneHelp" value="<?php echo $_SESSION['cotil_user_phone']; ?>" disabled>
                                            <small id="phoneHelp" class="form-text text-muted">Para alterar o telefone, acesse o menu <a href="profile.php">Perfil</a>.</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="email"><b>Email:</b></label>
                                            <input type="email" class="form-control" id="email" aria-describedby="phoneHelp" value="<?php echo $_SESSION['cotil_user_mail']; ?>" disabled>
                                            <small id="phoneHelp" class="form-text text-muted">Para alterar o email, acesse o menu <a href="profile.php">Perfil</a>.</small>
                                        </div>
                                        <br>
                                        <div id="RecaptchaField1"></div>
                                        <br>
                                        <button type="submit" class="btn btn-danger active">Enviar Solicitação</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="accordion" role="tablist" style="width: 100%;">
                    <div class="card">
                        <div class="card-header" role="tab" id="Node2">
                            <h5 class="mb-0">
                                <a href="#ConteudoNode2" data-toggle="collapse" data-parent="#accordion" class="text-danger">Monitoria de Humanas</a>
                            </h5>
                        </div>
                        <div class="collapse" role="tabpanel" id="ConteudoNode2">
                            <div class="card-block">
                                <form name="humanas" onsubmit="">
                                    <div class="form-group">
                                        <label for="doc2"><b>Selecione uma matéria:</b></label>
                                        <select class="form-control" id="doc2" name="doc2" onchange="updateSelectMateria(this.value,2)" required>
                                            <option disabled selected value>Selecione uma matéria</option>
                                            <option value="9"> Artes</option>
                                            <option value="1"> Filosofia</option>
                                            <option value="2"> Geografia</option>
                                            <option value="3"> História</option>
                                            <option value="6"> Língua Portuguesa</option>
                                            <option value="7"> Língua Inglesa</option>
                                            <option value="8"> Língua Espanhola</option>
                                            <option value="4"> Literatura</option>
                                            <option value="5"> Sociologia</option>
                                        </select>
                                    </div><br>
                                    <p>
                                        <b>Escolha os itens a seguir conforme a preferência de horário ou de monitor:</b><br>
                                        <small>(A escolha de um monitor limita as escolhas de horário, assim como a escolha de horário limita a escolha de monitores.)</small>
                                    </p>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="doc2"><b>Escolha um monitor:</b></label>
                                            <select class="form-control" id="select2monitor" name="select2monitor">
                                                <option disabled selected value>Selecione primeiramente uma matéria</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="doc2"><b>Escolha um horário:</b></label>
                                            <select class="form-control" id="doc2" name="doc2">
                                                <option disabled selected value>Selecione primeiramente uma matéria</option>
                                            </select>
                                        </div>
                                    </div><br>
                                    <div class="form-group">
                                        <label for="nome"><b>Escreva sobre o assunto monitoria:</b></label>
                                        <textarea class="form-control" rows="3" id="comment"></textarea>
                                        <small id="nameHelp" class="form-text text-muted">Cite todas as matérias e temas.</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone"><b>Telefone:</b></label>
                                        <input type="phone" class="form-control" id="phone" aria-describedby="phoneHelp" value="<?php echo $_SESSION['cotil_user_phone']; ?>" disabled>
                                        <small id="phoneHelp" class="form-text text-muted">Para alterar o telefone, acesse o menu <a href="profile.php">Perfil</a>.</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="email"><b>Email:</b></label>
                                        <input type="email" class="form-control" id="email" aria-describedby="phoneHelp" value="<?php echo $_SESSION['cotil_user_mail']; ?>" disabled>
                                        <small id="phoneHelp" class="form-text text-muted">Para alterar o email, acesse o menu <a href="profile.php">Perfil</a>.</small>
                                    </div>
                                    <br>
                                    <div id="RecaptchaField2"></div>
                                    <br>
                                    <button type="submit" class="btn btn-danger active">Enviar Solicitação</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php

            if(isset($_SESSION['cotil_periodo'])) {
                if($_SESSION['cotil_periodo']==1){

                } else {

                }
            } else {
                refresh("./index.php");
                exit();
            }
            ?>
        </div>
    </div>
</div>

<?php include './assets/include/footer.inc';?>
<!--TODO: Refazer o footer, achar outro design daorinha-->
<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/tether.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script type="text/javascript">
    function updateSelectMateria(str,depto) {
        if (str == "") {
            if(depto==1) {
                alert('1');
                document.getElementById("selectmonitor").innerHTML = "<option disabled selected value>Selecione primeiramente uma matéria</option>";
            } else {
                alert('2');
                document.getElementById("select2monitor").innerHTML = "<option disabled selected value>Selecione primeiramente uma matéria</option>";
            }
            return;
        } else {
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    if(depto==1) {
                        alert('3');
                        document.getElementById("selectmonitor").innerHTML = this.responseText;
                    } else {
                        alert('4');
                        document.getElementById("select2monitor").innerHTML = this.responseText;
                    }
                }
            };
            if(depto==1){
                alert('5');
                xmlhttp.open("GET","./assets/include/addselect_ajax/schedulemedio.php?dept=1&mat="+str,true);
            } else {
                alert('6');
                xmlhttp.open("GET","./assets/include/addselect_ajax/schedulemedio.php?dept=2&mat="+str,true);
            }
            xmlhttp.send();
        }
    }
    var CaptchaCallback = function() {
        grecaptcha.render('RecaptchaField1', {'sitekey' : '6LcyCSkUAAAAAIdUUIUagUX3JpfMGvZp04WVZlUH'});
        grecaptcha.render('RecaptchaField2', {'sitekey' : '6LcyCSkUAAAAAIdUUIUagUX3JpfMGvZp04WVZlUH'});
    };
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
</body>
</html>