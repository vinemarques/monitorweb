<?php
session_start();
function refresh($site){
    echo "<script type=\"text/javascript\">window.location.href = '".$site."';</script>";
}
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="../assets/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="../assets/css/style_login.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/unstyle.css">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Monitorias - Colégio Técnico de Limeira</title>
</head>
<body>
<?php

require_once './header-local.inc';

if(isset($_SESSION['cotil_lvl'])){
    if($_SESSION['cotil_lvl']!=4){
        if($_SESSION['cotil_lvl']==1){
            refresh("firstpass.php");
        } else if($_SESSION['cotil_lvl']==0){
            refresh("first.php");
        } else if($_SESSION['cotil_lvl']==2){
            refresh("firstdetails.php");
        } else {
            refresh("../login.php");
        }
    }
} else {
    refresh("../login.php");
    exit();
}
?>
<div class='container-fluid' id='main-container'>
        <div class='jumbotron' style='background-color: transparent'>
            <br>
            <center><h3>Verifique seu email</h3></center><br><br>
            <div style='text-align: justify;'>
                <p>Um email foi enviado para <b><?php echo $_SESSION['cotil_ne']; ?></b>.</p>
                <p>Para continuar no painel, precisamos que verifique sua caixa de emails e confirme a sua conta.<br><br></p>
                <p><center><a href='../assets/logout.php'><button type='button' class='btn btn-success'>Finalizar</button></a></center></p>
                <br><br>
            </div>
        </div>
        <!-- TODO: Arrumar alinhamento do footer -->
    </div>

<?php include '../assets/include/footer.inc';?>

<script src="../assets/js/jquery-3.1.1.js"></script>
<script src="../assets/js/tether.js"></script>
<script src="../assets/js/bootstrap.js"></script>
</body>
</html>