<?php
session_start();
function refresh($site){
    echo "<script type=\"text/javascript\">window.location.href = '".$site."';</script>";
}
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>
    <link rel="shortcut icon" href="../assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="../assets/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="../assets/css/style_login.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/unstyle.css">
    <title>Monitorias - Colégio Técnico de Limeira</title>
    <script src="../assets/js/jquery-3.1.1.js"></script>
    <script src="../assets/js/tether.js"></script>
    <script src="../assets/js/bootstrap.js"></script>
    <script src="../assets/js/jquery.mask.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function(){
            $('.telefone').mask('(99) 99999-9999');
        });
        function isValidEmail(email) {
            return /^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/.test(email)
                && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(email);
        };
        function premail()
        {
            if(isValidEmail(document.getElementById('email').value)){
                return true;
            } else {
                $('#alert_placeholder').html('<div class="container-fluid" id="main-container" style="padding-left: 0%; padding-right: 0%;"> <br> <div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Opa!</strong> O email inserido é inválido </div> </div>');
                return false;
            }
        };
    </script>
</head>
<body>
<?php

require_once './header-local.inc';

if((isset($_SESSION['cotil_lvl']))&&($_SESSION['cotil_user_status']==0)){
    if($_SESSION['cotil_lvl']!=2){
        if($_SESSION['cotil_lvl']==1){
            refresh("firstpass.php");
        } else if($_SESSION['cotil_lvl']==0){
            refresh("first.php");
        } else if($_SESSION['cotil_lvl']==3){
            refresh("firstfinish.php");
        } else {
            refresh("../login.php");
        }
    }
} else {
    if($_SESSION['cotil_user_status']==2)
    {
        refresh("firstfinish.php");
        exit();
    } else {
        refresh("../login.php");
        exit();
    }
}

if(isset($_REQUEST['alt'])){
    if($_REQUEST['alt']==2){
        echo "<script type='text/javascript'>
                 var r = confirm('Não será possível acessar o painel sem continuar.\\nDeseja mesmo cancelar?');
                 if (r == true)
                    { window.location.href = '../assets/logout.php'; }
              </script>";
    } else if($_REQUEST['alt']==1){
        $_SESSION['cotil_lvl']=1;
        echo "<script type='text/javascript'>
                 var r = confirm('Deseja mesmo retornar?\\n Os dados inseridos não serão salvos');
                 if (r == true)
                    { window.location.href = 'firstpass.php'; }
              </script>";
    } else if($_REQUEST['alt']==3){
        require_once '../assets/include/connection.inc';
        $_SESSION['cotil_lvl']=4;
        $_SESSION['cotil_ne']=$_REQUEST['email'];
        $_SESSION['cotil_np']=$_REQUEST['tel'];
        $uid = md5(uniqid(rand(), false));
        $query = "UPDATE Alunos SET senha='".$_SESSION['cotil_sds']."',uid='".$uid."',telefone='".$_SESSION['cotil_np']."',email='".$_SESSION['cotil_ne']."',status=2 WHERE ra='".$_SESSION['cotil_user_ra']."'";
        try
        {
            $stmt = $conn->prepare($query);
            $stmt->execute();
            refresh("firstsend.php?userid=".$uid);
        }
        catch (PDOException $err) {
            echo "<div class='container-fluid' id='main-container' style='padding-left: 0%; padding-right: 0%;'> <br>
                    <div class='alert alert-warning'>
                     <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <strong>Erro!</strong> Não foi possível gravar no banco de dados.
                     </div>";
        }
    } else {
        refresh("../index.php");
    }
}

?>
    <div id="alert_placeholder"></div>
    <div class='container-fluid' id='main-container'>
        <div class='jumbotron' style='background-color: transparent'>
            <br>
            <center><h3>Inclusão de e-mail e telefone</h3></center><br>
            <div style='text-align: justify;'>
                <p>Email e telefone<b> bla bla bla</b>. bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla.</p>
                <!--// TODO: Separar cards-->

                <div class='card row' style='padding-top: 20px; padding-bottom: 10px; padding-right: 20px; padding-left: 20px; border-color: #ED3237;'>
                    <div class='row'>
                        <div class='col-xs-12 col-sm-12 col-md-12'>
                            <form role='form' name='changemailphone' onsubmit="return premail()" method='post' action='?alt=3'>
                                <div class='row'>
                                    <div class='col-xs-6 col-sm-6 col-md-6'>
                                        <div class='form-group'>
                                            <label for='email'><b>Insira um e-mail:</b></label>
                                            <input type='email' id='email' name='email' style='border-color: #ED3237;' minlength='8' class='form-control input-lg' placeholder='aluno@unicamp.br' tabindex='1' required autofocus>
                                        </div>
                                    </div>
                                    <div class='col-xs-6 col-sm-6 col-md-6'>
                                        <div class='form-group'>
                                            <label for='tel'><b>Insira um telefone:</b></label>
                                            <input type='text' name='tel' style='border-color: #ED3237;' minlength='15' class='form-control input-lg telefone' placeholder='(19) 99999-9999'  tabindex='2' required>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>

                <br>
                <center>
                    <p><b>Pronto(a) para continuar?</b></p>
                    <p>
                        <a href='?alt=1'><button type='button' class='btn btn-warning' tabindex="4">Retornar</button></a>
                        <a href='?alt=2'><button type='button' class='btn btn-danger' tabindex="5">Cancelar</button></a>
                        <button type='submit' class='btn btn-success' tabindex="3">Continuar</button>
                    </p>
                </center>
            </div>
        </div>
    </div>
    </form>
    <!-- TODO: Arrumar alinhamento do footer -->
<?php include '../assets/include/footer.inc';?>
</body>
</html>