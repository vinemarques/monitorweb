<?php
session_start();
function refresh($site){
    echo "<script type=\"text/javascript\">window.location.href = '".$site."';</script>";
}
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="../assets/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="../assets/css/style_login.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/unstyle.css">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Monitorias - Colégio Técnico de Limeira</title>
</head>
<body>
<?php

require_once './header-local.inc';

if(isset($_REQUEST['return'])&&($_REQUEST['return']==1)) {
    $_SESSION['cotil_lvl']=1;
    $_SESSION['cotil_sds']=NULL;
}

if((isset($_SESSION['cotil_lvl']))&&($_SESSION['cotil_user_status']==0)){
    if($_SESSION['cotil_lvl']!=1){
        if($_SESSION['cotil_lvl']==0){
            refresh("first.php");
        } else if($_SESSION['cotil_lvl']==2){
            refresh("firstpass.php");
        } else if($_SESSION['cotil_lvl']==3){
            refresh("firstdetails.php");
        } else if($_SESSION['cotil_lvl']==4){
            refresh("firstfinish.php");
        } else {
            refresh("../login.php");
        }
    }
} else {
    if($_SESSION['cotil_user_status']==2)
    {
        refresh("firstfinish.php");
        exit();
    } else {
        refresh("../login.php");
        exit();
    }
}

if(isset($_REQUEST['alt'])){
    if($_REQUEST['alt']==2){
        echo "<script type='text/javascript'>
                 var r = confirm('Não será possível acessar o painel sem continuar.\\nDeseja mesmo cancelar?');
                 if (r == true)
                    { window.location.href = '../assets/logout.php'; }
              </script>";
    } else if($_REQUEST['alt']==1){
        $_SESSION['cotil_lvl']=0;
        echo "<script type='text/javascript'>
                 var r = confirm('Deseja mesmo retornar?\\n Os dados inseridos não serão salvos');
                 if (r == true)
                    { window.location.href = 'first.php'; }
              </script>";
    } else if($_REQUEST['alt']==3){
        if($_REQUEST['pass1']==$_REQUEST['pass2']){
            $_SESSION['cotil_sds']=md5($_REQUEST['pass1']."batata");
            $_SESSION['cotil_lvl']=2;
            refresh("firstdetails.php");
        } else {
            echo "<div class='container-fluid' id='main-container' style='padding-left: 0%; padding-right: 0%;'> <br>
                    <div class='alert alert-danger'>
                     <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                     <strong>Opa!</strong> Senhas inseridas diferem.
                    </div>
                  </div>";
        }
    } else {
        refresh("../index.php");
    }
}

?>
    <div class='container-fluid' id='main-container'>
        <div class='jumbotron' style='background-color: transparent; padding-top: 20px;'> <br>
            <center><h3>Alteração da senha</h3></center><br>
            <div style='text-align: justify;'>
            <p>
                A senha padrão de login é muito simples e, por motivos de segurança,
                <b> nós pedimos para você trocar a sua senha</b>.
                Uma senha forte contém pelo menos 8 caracteres, contendo letras, números e caracteres especiais.
            </p>
            <section id='card card-deck'>
                <div class='card row' style='padding-top: 20px; padding-right: 20px; padding-left: 20px; border-color: #ED3237;'>
                    <div class='row'>
                        <div class='col-xs-12 col-sm-12 col-md-12'>
                            <form role='form' name='changepasswd' method='post' action='?alt=3'>
                                <div class='row'>
                                    <div class='col-xs-6 col-sm-6 col-md-6'>
                                        <div class='form-group'>
                                            <label for='pass2'><b>Nova Senha:</b></label>
                                            <input type='password' name='pass1' style='border-color: #ED3237;' minlength='8' class='form-control input-lg' placeholder='Senha' tabindex='1' required autofocus>
                                        </div>
                                    </div>
                                    <div class='col-xs-6 col-sm-6 col-md-6'>
                                        <div class='form-group'>
                                            <label for='pass2'><b>Confirmar Senha:</b></label>
                                            <input type='password' name='pass2' style='border-color: #ED3237;' minlength='8' class='form-control input-lg' placeholder='Confirmar Senha' tabindex='2' required>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            </section>
            <br>
            <center><p><b>Pronto(a) para continuar?</b></p>
                <p>
                    <a href='?alt=1'><button type='button' class='btn btn-warning' tabindex="4">Retornar</button></a>
                    <a href='?alt=2'><button type='button' class='btn btn-danger' tabindex="5">Cancelar</button></a>
                    <button type='submit' class='btn btn-success' tabindex="3">Continuar</button>
                </p>
            </center>
        </div>
    </div>
    </form>
    <!-- TODO: Arrumar alinhamento do footer -->
<?php include '../assets/include/footer.inc';?>

<script src="../assets/js/jquery-3.1.1.js"></script>
<script src="../assets/js/tether.js"></script>
<script src="../assets/js/bootstrap.js"></script>
</body>
</html>