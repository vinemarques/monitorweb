<?php
session_start();

function refresh($site){
    echo "<script type=\"text/javascript\">window.location.href = '".$site."';</script>";
}

?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="../assets/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="../assets/css/style_login.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/unstyle.css">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Monitorias - Colégio Técnico de Limeira</title>
</head>
<body>
<?php

require_once './header-local.inc';

if(isset($_REQUEST['return'])&&($_REQUEST['return']==1)) {
    $_SESSION['cotil_lvl']=0;
}

if((isset($_SESSION['cotil_lvl']))&&($_SESSION['cotil_user_status']==0)){
    if($_SESSION['cotil_lvl']!=0){
        if($_SESSION['cotil_lvl']==1){
            refresh("firstpass.php");
        } else if($_SESSION['cotil_lvl']==2){
            refresh("firstdetails.php");
        } else if($_SESSION['cotil_lvl']==3){
            refresh("firstfinish.php");
        } else {
            refresh("../login.php");
        }
    }
} else {
    if($_SESSION['cotil_user_status']==2)
    {
        refresh("firstfinish.php");
        exit();
    } else {
        refresh("../login.php");
        exit();
    }
}

if(isset($_REQUEST['alt'])){
    if($_REQUEST['alt']==1){
        echo "<script type='text/javascript'>
                        var r = confirm('Não será possível acessar o painel sem continuar.\\nDeseja mesmo cancelar?');
                            if (r == true)
                            { window.location.href = '../assets/logout.php'; }
                      </script>";
    } else if($_REQUEST['alt']==2){
        $_SESSION['cotil_lvl']=1;
        refresh("firstpass.php");
        exit();
    } else {
        refresh("../index.php");
    }
}

?>

<div class='container-fluid' id='main-container'>
        <div class='jumbotron' style='background-color: transparent'>
        <br><center><h3>Bem-Vindo(a), <?php echo $_SESSION['cotil_user_nome']; ?>!</h3></center><br>
            <div style='text-align: justify;'>
                <p><b>Esse é o seu primeiro login no painel de monitorias.</b> Aqui você vai poder verificar os horários das monitorias, marcar uma monitoria, participar de uma monitoria já cadastrada ou até agendar uma monitoria em grupo. Legal, né?</p>
                <p>Antes de fazer o que precisa, <b>precisamos que crie uma nova senha, nos informe um email e um telefone para contato</b>, tudo para facilitar a vida dos monitores quando você resolver marcar uma monitoria.</p>
                <br>
                <center>
                    <p><b>Pronto(a) para continuar?</b></p>
                    <p>
                        <a href='?alt=1'><button type='button' class='btn btn-danger'>Cancelar</button></a>
                        <a href='?alt=2'><button type='button' class='btn btn-success'>Continuar</button></a>
                    </p>
                </center>
            </div>
        </div>
    </div>
    <!-- TODO: Arrumar alinhamento do footer -->
<?php include '../assets/include/footer.inc';?>

<script src="../assets/js/jquery-3.1.1.js"></script>
<script src="../assets/js/tether.js"></script>
<script src="../assets/js/bootstrap.js"></script>
</body>
</html>