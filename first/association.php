<?php
session_start();
function refresh($site){
    echo "<script type=\"text/javascript\">window.location.href = '".$site."';</script>";
}
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../assets/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="../assets/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="../assets/css/style_login.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/unstyle.css">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Monitorias - Colégio Técnico de Limeira</title>
</head>
<body>
<?php

require_once './header-local.inc';

if (isset($_SESSION['preauth'])&&!isset($_REQUEST['alt'])) {
    if($_SESSION['preauth']=='g'){
        $conta = "Google";
    } else {
        $conta = "Facebook";
    }
} elseif(isset($_SESSION['preauth'])&&isset($_REQUEST['alt'])) {
    if ($_REQUEST['alt'] == 2) {
        echo "<script type='text/javascript'>
                 var r = confirm('Não será possível acessar o painel sem continuar.\\nDeseja mesmo cancelar?');
                 if (r == true)
                    { window.location.href = '../assets/logout.php'; }
              </script>";
    } else if ($_REQUEST['alt'] == 1) {
        echo "<script type='text/javascript'>
                 var r = confirm('Deseja mesmo retornar?\\n Os dados inseridos não serão salvos');
                 if (r == true)
                    { window.location.href = 'login.php'; }
              </script>";
    } else if ($_REQUEST['alt'] == 3) {
        include '../assets/include/connection.inc';
        $login = isset($_REQUEST['login']) ? $_REQUEST['login'] : null;
        $pass = isset($_REQUEST['pass']) ? $_REQUEST['pass'] : null;
        $pass = md5($pass . "batata");

        $sql = "SELECT * FROM Alunos WHERE ra = :login AND senha = :pass";

        $stmt = $conn->prepare($sql);

        $stmt->bindParam("login", $login);
        $stmt->bindParam("pass", $pass);

        if ($stmt->execute()) {
            if ($registro = $stmt->fetch(PDO::FETCH_OBJ)) {
                $_SESSION['cotil_user_ra'] = $registro->ra;
                $_SESSION['cotil_user_nome'] = $registro->nome;
                $_SESSION['cotil_user_status'] = $registro->status;

                if($_SESSION['preauth']=='g'){
                    $query="UPDATE Google_Auth SET cotil_ra = ".$registro->ra." WHERE google_id = ".$_SESSION['preauthid'];
                } else {
                    $query="UPDATE Facebook_Auth SET cotil_ra = ".$registro->ra." WHERE app_id = ".$_SESSION['preauthid'];
                }
                $statement = $conn->prepare($query);
                $statement->execute();

                if ($_SESSION['cotil_user_status'] == 0) {
                    $_SESSION['cotil_lvl'] = 0;
                    refresh("./first/first.php");
                    exit();
                } else if ($_SESSION['cotil_user_status'] == 2) {
                    $_SESSION['cotil_lvl'] = 4;
                    $_SESSION['cotil_ne'] = $registro->email;
                    refresh("./first/firstfinish.php");
                    exit();
                } else {
                    switch ($registro->curso[1]) {
                        case 1:
                            $_SESSION['cotil_user_tec'] = "Informática";
                            break;
                        case 2:
                            $_SESSION['cotil_user_tec'] = "Mecânica";
                            break;
                        case 3:
                            $_SESSION['cotil_user_tec'] = "Edificações";
                            break;
                        case 4:
                            $_SESSION['cotil_user_tec'] = "Geodésia e Cartografia";
                            break;
                        case 5:
                            $_SESSION['cotil_user_tec'] = "Enfermagem";
                            break;
                        case 6:
                            $_SESSION['cotil_user_tec'] = "Qualidade";
                            break;
                    }
                    if ($registro->sala != 000) {
                        if ($registro->sala[0] == 1) {
                            $_SESSION['cotil_user_sala'] = $registro->sala[1] . "." . $registro->sala[2] . " Diurno";
                        } else {
                            $_SESSION['cotil_user_sala'] = $registro->sala[1] . "." . $registro->sala[2] . " Noturno";
                        }
                    }
                    $_SESSION['cotil_lvl'] = 5;
                    refresh("../index.php");
                    exit();
                }
            } else {
                echo "<script type='text/javascript'>alert('Usuário ou senha incorretos! Tente Novamente.');</script>";
                refresh("association.php");
            }
        } else {
            echo "Falha de acesso ao BD!<br>";
        }
    } else {
        refresh("index.php");
    }
} else {
    refresh("../login.php");
}

?>
<div class='container-fluid' id='main-container'>
    <div class='jumbotron' style='background-color: transparent'>
        <br>
        <center><h3>Vincular Contas</h3></center><br>
        <div style='text-align: justify;'>
            <p>
                Sua conta do <?php echo $conta; ?> não tem nenhum RA ou cadastro associado. Para continuar no painel,
                faça o login com o seu usuário e senha do painel.
            </p>
            <section id='card card-deck'>
                <div class='card row' style='padding-top: 20px; padding-right: 20px; padding-left: 20px; border-color: #ED3237;'>
                    <div class='row'>
                        <div class='col-xs-12 col-sm-12 col-md-12'>
                            <form role='form' name='changepasswd' method='post' action='?alt=3'>
                                <div class='row'>
                                    <div class='col-xs-6 col-sm-6 col-md-6'>
                                        <div class='form-group'>
                                            <label for="login"><b>Usuário:</b></label>
                                            <input type="text" style="border-color: #ED3237" class="form-control" name="login" id="login" placeholder="Email, RA ou Eduroam" required autofocus>
                                        </div>
                                    </div>
                                    <div class='col-xs-6 col-sm-6 col-md-6'>
                                        <div class='form-group'>
                                            <label for="senha"><b>Senha:</b></label>
                                            <input type="password" style="border-color: #ED3237" class="form-control" name="pass" id="senha" placeholder="Senha" required>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
        </div>
        </section>
        <br>
        <center><p><b>Pronto(a) para continuar?</b></p>
            <p>
                <a href='?alt=1'><button type='button' class='btn btn-warning'>Retornar</button></a>
                <a href='?alt=2'><button type='button' class='btn btn-danger'>Cancelar</button></a>
                <button type='submit' class='btn btn-success'>Continuar</button>
            </p>
        </center>
    </div>
</div>
</form>
<!-- TODO: Arrumar alinhamento do footer -->
<?php include '../assets/include/footer.inc';?>

<script src="../assets/js/jquery-3.1.1.js"></script>
<script src="../assets/js/tether.js"></script>
<script src="../assets/js/bootstrap.js"></script>
</body>
</html>