<?php
session_start();
function refresh($site){
    echo "<script type=\"text/javascript\">window.location.href = '".$site."';</script>";
}
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../assets/include/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="../assets/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="../assets/css/style_login.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/unstyle.css">
    <script src="https://use.fontawesome.com/01f6939522.js"></script>

    <title>Monitorias - Colégio Técnico de Limeira</title>
</head>
<body>
<?php

    /*if(isset($_SESSION['cotil_lvl'])){
        if($_SESSION['cotil_lvl']!=4){
            if($_SESSION['cotil_lvl']==1){
                refresh("firstpass.php");
            } else if($_SESSION['cotil_lvl']==0){
                refresh("first.php");
            } else if($_SESSION['cotil_lvl']==2){
                refresh("firstdetails.php");
            } else {
                refresh("login.php");
            }
        }
    } else {
        refresh("login.php");
        exit();
    }*/

    if(!isset($_REQUEST['va'])) {
        $email = $_SESSION['cotil_ne'];
        $uid = $_REQUEST['userid'];
        $conteudo = "
        <h1>Bem vindo ao Painel de Monitoria!</h1>
                Valide seu email acessando o link: 
                <a href='http://lab.vinemarques.tk/infd35/cotil/monitoria/first/firstsend.php?userid=".$uid."&va=true'>AQUI</a>
        ";

        //require_once './header-local.inc';
        require_once '../assets/mail/PHPMailerAutoload.php';
        require_once '../assets/include/connection.inc';

        date_default_timezone_set('Etc/UTC');
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->isHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "projetodinfo@gmail.com";
        $mail->Password = "projeto#01";
        $mail->setFrom('projetodinfo@gmail.com', 'Monitoria - Colégio Técnico de Limeira');
        $mail->addReplyTo('vinemaarques@gmail.com', 'Vinicius Marques');
        $mail->addAddress($email);
        $mail->Subject = 'Confirmação de conta - Monitoria COTIL';
        $mail->msgHTML($conteudo);
        $mail->CharSet='UTF-8';
        $mail->AltBody = 'Algo deu errado no momento em que tentamos gerar seu email D:';
        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            refresh("firstfinish.php");
        }
    } else {
        require_once '../assets/include/connection.inc';
        $query = "UPDATE Alunos SET status=1 WHERE uid='".$_REQUEST['userid']."'";
        $stmt = $conn->prepare($query);
        if($stmt->execute()){
            require_once './header-local.inc';
            echo "  <div class='container-fluid' id='main-container'>
                        <div class='jumbotron' style='background-color: transparent'>
                            <br>
                            <center><h3>Email confirmado com sucesso!</h3></center><br><br>
                            <div style='text-align: justify;'>
                                <p>Seu email foi confirmado, agora você já pode acessar o painel e usufruir de todos os recursos do sistema.</p>
                                <p>Para continuar no painel, finalize a confirmação e faça seu login.<br><br></p>
                                <p><center><a href='../assets/logout.php'><button type='button' class='btn btn-success'>Finalizar</button></a></center></p>
                                <br><br>
                            </div>
                        </div>
                    </div>";
            require_once '../assets/include/footer.inc';
        } else {
            require_once './header-local.inc';
            echo " <div class='container-fluid' id='main-container'>
                        <div class='jumbotron' style='background-color: transparent'>
                            <br>
                            <center><h3>Alguma coisa deu errado</h3></center><br><br>
                            <div style='text-align: justify;'>
                                <p>Sua conta já foi verificada ou seu link é inválido</p>
                                <p>Para continuar no painel, finalize a confirmação e faça seu login.<br><br></p>
                                <p><center><a href='../assets/logout.php'><button type='button' class='btn btn-success'>Finalizar</button></a></center></p>
                                <br><br>
                            </div>
                        </div>
                    </div>";
            require_once '../assets/include/footer.inc';
        }
    }

?>

</body>
</html>